# Generator / Co / Koa

Test project using ES6 generators, Co and Koa

## Setup

### Install Yarn

[https://yarnpkg.com/en/docs/install](https://yarnpkg.com/en/docs/install)

### Install Babel

[https://babeljs.io/docs/setup/#installation](https://babeljs.io/docs/setup/#installation)

### Clone project

`git clone git@gitlab.com:anthony-ly/es6-generator.git`

### Install dependencies

`yarn install`

### Start

`npm start`

[http://localhost:3000/posts](http://localhost:3000/posts)

[http://localhost:3000/comments](http://localhost:3000/comments)

## Licence

MIT