import co from 'co';
import koa from 'koa';
import fetch from 'isomorphic-fetch';
import Router from 'koa-router';

const root = 'https://jsonplaceholder.typicode.com';
const get = (uri) => ( fetch(root + uri).then(response => (response.json())) )

const Post = {
  findAll: () => {
    return get('/posts')
  }
}

const Comment = {
  findAll: () => {
    return get('/comments')
  }
}

const app = koa();
const router = Router();

router.get('/posts', function *(next){
  this.body = yield Post.findAll();
  yield next
});

router.get('/comments', function *(next){
  this.body = yield Comment.findAll();
  yield next
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);